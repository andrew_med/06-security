<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
<style>
table, th, td {
  border: 1px solid gray;
  border-collapse: collapse;
}
th, td {
  padding: 10px;
}
</style>
</head>

<body>

<%@ include file = "../header.jsp" %>

<h1>Project Management</h1>

<table>
  <tr>
    <th>ID</th>
    <th>Name</th>
  </tr>
  <c:forEach items="${projects}" var="project">
  <tr>
    <td>${project.id}</td>
    <td>${project.name}</td>
    <td><a href="edit?id=${project.id}">Edit</a></td>
    <td><a href="remove/${project.id}">Remove</a></td>
  </tr>
  </c:forEach>
</table>

<p><a href="edit">New Project</a></p>
<p><a href="list">Refresh</a></p>

</body>
</html>
