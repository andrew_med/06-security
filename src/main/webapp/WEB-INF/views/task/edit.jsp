<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<body>

<%@ include file = "../header.jsp" %>

<h1>Edit Task</h1>

<form:form action="save" method="post" modelAttribute="task">
  <p>ID:<br/><form:input path="id" readonly="true"/></p>
  <p>Name:<br/><form:input path="name"/></p>
  <p><input type="submit" value="Save"/></p>
</form:form>

</body>
</html>
