<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<ul style="list-style: none; padding-left: 0;">
<li style="display: inline-block;"> <a href="${pageContext.request.contextPath}/">Main</a> </li>
<sec:authorize access="hasRole('ROLE_PROJECT_MANAGER')">
    <li style="display: inline-block;"> <a href="${pageContext.request.contextPath}/project/list">Projects</a> </li>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_TASK_MANAGER')">
    <li style="display: inline-block;"> <a href="${pageContext.request.contextPath}/task/list">Tasks</a> </li>
</sec:authorize>
<li style="display: inline-block;"> <a href="${pageContext.request.contextPath}/logout">Logout</a> </li>
</ul>
