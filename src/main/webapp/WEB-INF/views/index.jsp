<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<body>

<%@ include file = "header.jsp" %>

<h1>Welcome!</h1>

<sec:authorize access="hasRole('ROLE_PROJECT_MANAGER')">
    <p><a href="${pageContext.request.contextPath}/project/list">Project Management</a></p>
</sec:authorize>

<sec:authorize access="hasRole('ROLE_TASK_MANAGER')">
    <p><a href="${pageContext.request.contextPath}/task/list">Task Management</a></p>
</sec:authorize>

</body>
</html>
