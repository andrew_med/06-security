package ru.volnenko.se.repository;

import ru.volnenko.se.entity.Task;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Denis Volnenko
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, String> {
}
