package ru.volnenko.web.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.TaskRepository;

@Controller
@RequestMapping("/task")
public class TaskController {
	
    @Autowired
    private TaskRepository taskRepository;
	
    @GetMapping("/list")
    public String list(Model model) {
    	model.addAttribute("tasks", taskRepository.findAll());
        return "task/list";
    }
    
    @GetMapping("/remove/{id}")
    public String remove(@PathVariable String id) {
    	taskRepository.deleteById(id);
        return "redirect:/task/list";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam(required = false) String id, Model model) {
    	Task task = (id == null) ? null : taskRepository.findById(id).orElse(null);
    	if(task == null) task = new Task();
    	model.addAttribute("task", task);
        return "task/edit";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("task") Task task) {
    	taskRepository.save(task);
        return "redirect:/task/list";
    }
}
