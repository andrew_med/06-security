package ru.volnenko.web.rest;

import ru.volnenko.se.entity.Project;
import ru.volnenko.se.repository.ProjectRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/project")
public final class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/list")
    public List<Project> getList() {
    	return projectRepository.findAll();
    }

    @PostMapping
    public Project save(@RequestBody Project project) {
    	return projectRepository.save(project);
    }
    
    @GetMapping("/{id}")
    public Project get(@PathVariable String id) {
    	return projectRepository.findById(id).orElse(null);
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable String id) {
    	projectRepository.deleteById(id);
    }
}
