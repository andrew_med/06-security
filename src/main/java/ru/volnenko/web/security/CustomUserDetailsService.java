package ru.volnenko.web.security;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ru.volnenko.se.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
	
	public static final String ROLE_PROJECT_MANAGER = "ROLE_PROJECT_MANAGER";

	public static final String ROLE_TASK_MANAGER = "ROLE_TASK_MANAGER";
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		
		 ru.volnenko.se.entity.User user = userRepository.findByLogin(login);
		 if(user == null) {
			 throw new UsernameNotFoundException(login + " not found");
		 }
		 
		 Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
		 if(user.getRoles() != null) {
			 for(String role : user.getRoles().split(",")) {
				 role = role.trim();
				 if(!role.isEmpty()) {
					 roles.add(new SimpleGrantedAuthority(role));
				 }
			 }
		 }
		 
		 return new User(user.getLogin(), user.getPasswordHash(), roles);
	}
	
	@PostConstruct
	private void registerTestUsers() {
		String password = "123456";
		registerUser("test1", password, ROLE_PROJECT_MANAGER);
		registerUser("test2", password, ROLE_TASK_MANAGER);
		registerUser("test3", password, ROLE_PROJECT_MANAGER, ROLE_TASK_MANAGER);
	}
	
	private void registerUser(String login, String password, String... role) {
		ru.volnenko.se.entity.User user = userRepository.findByLogin(login);
		if(user != null) {
			System.out.println(login + " already exists");
		} else {
			user = new ru.volnenko.se.entity.User();
			user.setLogin(login);
			user.setPasswordHash(PASSWORD_ENCODER.encode(password));
			if(role.length > 0) {
				user.setRoles(String.join(", ", role));
			}
			userRepository.save(user);
		}
	}
}
