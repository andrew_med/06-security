package ru.volnenko.web.security;

import static ru.volnenko.web.security.CustomUserDetailsService.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@ComponentScan("ru.volnenko.web")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(PASSWORD_ENCODER);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/*").access("hasRole('"+ ROLE_PROJECT_MANAGER +"') or hasRole('" + ROLE_TASK_MANAGER + "')")
		.antMatchers("/project/**").access("hasRole('"+ ROLE_PROJECT_MANAGER +"')")
		.antMatchers("/task/**").access("hasRole('" + ROLE_TASK_MANAGER + "')")
		.and().formLogin();
    }
}
