package ru.volnenko.client;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

public class RestClient {

	private static final String URL_APP_ROOT = "http://localhost:8080/taskmanager";
	
	private static final String URL_REST_PROJECT = URL_APP_ROOT + "/rest/project";
	private static final String URL_REST_TASK = URL_APP_ROOT + "/rest/task";

	private static RestTemplate restTemplate = new RestTemplate();
	
	
	public static void main(String... args) {
		
		Project project = new Project();
		project.setName("Sample project");
		project = saveProject(project);
		projectList();

		project = getProject(project.getId());
		removeProject(project.getId());
		projectList();
		
		Task task = new Task();
		task.setName("Sample task");
		task = saveTask(task);
		taskList();

		task = getTask(task.getId());
		removeTask(task.getId());
		taskList();
	}
	
	private static List<Project> projectList() {
		Project[] projects = restTemplate.getForObject(URL_REST_PROJECT + "/list", Project[].class);
		System.out.println("\n project-list");
		for(Project project : projects) {
			System.out.println(project.getId() + ": " + project.getName()); 
		}
		return Arrays.asList(projects);
	}

	private static Project saveProject(Project project) {
		System.out.println("\n project-save: " + project.getName());
		return restTemplate.postForObject(URL_REST_PROJECT, project, Project.class);
	}
	
	private static Project getProject(String id) {
		Project project = restTemplate.getForObject(URL_REST_PROJECT + '/' + id, Project.class);
		System.out.println("\n project-get " + id + ": " + project.getName()); 
		return project;
	}
	
	private static void removeProject(String id) {
		System.out.println("\n project-remove " + id); 
		restTemplate.delete(URL_REST_PROJECT + '/' + id);
	}
	
	private static List<Task> taskList() {
		Task[] tasks = restTemplate.getForObject(URL_REST_TASK + "/list", Task[].class);
		System.out.println("\n task-list");
		for(Task task : tasks) {
			System.out.println(task.getId() + ": " + task.getName()); 
		}
		return Arrays.asList(tasks);
	}
	
	private static Task saveTask(Task task) {
		System.out.println("\n task-save: " + task.getName());
		return restTemplate.postForObject(URL_REST_TASK, task, Task.class);
	}
	
	private static Task getTask(String id) {
		Task task = restTemplate.getForObject(URL_REST_TASK + '/' + id, Task.class);
		System.out.println("\n task-get " + id + ": " + task.getName()); 
		return task;
	}
	
	private static void removeTask(String id) {
		System.out.println("\n task-remove " + id); 
		restTemplate.delete(URL_REST_TASK + '/' + id);
	}
}
